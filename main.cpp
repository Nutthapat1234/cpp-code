#include <iostream>
#include <string>
using namespace std;

class ListNode
{
public:
    ListNode(string v = "") : val(v), next(NULL) {}
    string getValue() const { return val; }
    ListNode *getPointer() const { return next; }
    void setValue(const string &v) { val = v; }
    void setPointer(ListNode *ptr) { next = ptr; }
    void display() const { cout << val; }

private:
    string val;
    ListNode *next;
};
//-----------------------------------------
class LinkedList
{
public:
    //constructor & destructor
    LinkedList();
    ~LinkedList();
    void pushBack(const string &val);
    void display() const;
    void clear();
    void insert(const string &val, ListNode *pos);
    void erase(ListNode *pos);
    bool operator==(const LinkedList *list);

private:
    ListNode *firstNode;
};

LinkedList::LinkedList()
{
    firstNode = NULL;
}
//--------------------------
LinkedList::~LinkedList()
{
    clear();
}
//--------------------------
void LinkedList::pushBack(const string &val)
{
    ListNode *newNode = new ListNode(val);

    if (firstNode == NULL)
    {
        firstNode = newNode;
    }
    else
    {
        ListNode *ptr = firstNode;
        while (ptr->getPointer() != NULL)
        {
            ptr = ptr->getPointer();
        }
        ptr->setPointer(newNode);
    }
}
//--------------------------
void LinkedList::display() const
{
    if (firstNode == NULL)
    {
        cout << "The linked-list is empty." << endl;
    }
    else
    {
        ListNode *ptr = firstNode;
        while (ptr != NULL)
        {
            ptr->display();
            cout << " ";
            ptr = ptr->getPointer();
        }
    }
    cout << endl;
}
//--------------------------
void LinkedList::clear()
{
    ListNode *ptr = firstNode;
    while (ptr->getPointer() != NULL)
    {
        ListNode *temp = ptr;
        ptr = ptr->getPointer();
        delete temp;
    }
}

void LinkedList::insert(const string &val, ListNode *pos)
{
    ListNode *ptr = firstNode;
    ListNode *temp = new ListNode();
    while (ptr->getPointer() != NULL)
    {
        if (ptr == pos)
        {
            temp->setValue(ptr->getValue());
            temp->setPointer(ptr->getPointer());
            ptr->setPointer(temp);
            ptr->setValue(val);
            return;
        }
        ptr = ptr->getPointer();
    }
    if (ptr == pos)
    {
        temp->setValue(ptr->getValue());
        ptr->setValue(val);
        ptr->setPointer(temp);
        temp->setPointer(NULL);
    }else{
        pos->setValue(val);
        ptr->setPointer(pos);
        pos->setPointer(NULL);
        delete temp;
    }
}

void LinkedList::erase(ListNode *pos)
{
    ListNode *ptr = firstNode;
    if (pos == firstNode)
    {
        firstNode = ptr->getPointer();
        delete pos;
        return;
    }
    while (ptr->getPointer() != NULL)
    {
        if (ptr->getPointer() == pos)
        {
            ptr->setPointer(ptr->getPointer()->getPointer());
            delete pos;
            return;
        }
        ptr = ptr->getPointer();
    }
    delete pos;
}
// main

int main()
{
    LinkedList list1;
    list1.pushBack("Red");
    list1.pushBack("Green");
    list1.pushBack("Blue");
    ListNode *pos = new ListNode();
    list1.insert("Yellow", pos);
    list1.display();
    list1.insert("Black", pos);
    list1.erase(pos);
    cout << "List 1:" << endl;
    list1.display();
    return 0;
}
